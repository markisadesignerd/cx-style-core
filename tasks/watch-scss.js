'use strict';

const path = require('path');
const fs = require('fs');
const sass = require('node-sass');
const config = require('../config');
const importOnce = require('node-sass-import-once');

sass.render({
  file: path.join(config.srcDir, 'crosschux.scss'),
  outputStyle: 'compressed',
  outFile: path.join(config.buildDir),
  sourceMap: true,
  includePaths: [path.join(config.rootDir, 'node_modules')],
  importer: importOnce,
  importOnce: {
    index: true,
    css: true,
  },
}, (error, result) => {
  if (!error) {
    // No errors during the compilation, write this result on the disk
    fs.writeFile(path.join(config.buildDir, 'crosschux.css'), result.css, (err) => {
      if (!err) {
        //file written on disk
      }
    });
    fs.writeFile(path.join(config.buildDir, 'crosschux.css.map'), result.map, (err) => {
      if (!err) {
        //file written on disk
      }
    });
  }
});
