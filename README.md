CrossChUX Style: Core
===

####This repo contains the core components of the CrossChUX styling implementation.

Installation
---

To install as a dependency, run:    
`npm install git@bitbucket.org:crosschx/cx-style-core.git --save`
            
There are two options for usage:

#### As .scss Imports:

If you only need the variables, functions, mixins, and placeholders, import `sole.scss` in your project:  
    
    @import '~cx-style-core/src/sole.scss';

If you want everything, including contextual helper classes, import `crosschux.scss` into your project:  
    
    @import '~cx-style-core/src/crosschux.scss';

If you need more granularity, feel free to mix and match imports from the `crosschux/src` directory.

*Note: This assumes you&rsquo;re using Webpack's node_modules-relative importer by prefacing the `@imports` with a `~` character*

#### As Compiled .css:  

Import the compiled css into the project and let Webpack handle the rest:

    @import '~cx-style-core/dist/crosschux.css';    

*Note: This assumes you're using Webpack's node_modules-relative importer by prefacing the `@imports` with a `~` character*


Development
---

1. Clone down the repo and run 

    `npm install`

2. Branch, work, make a PR, etc. To have the Webpack server watch for changes, run:

    `npm run serve`


Basic Documentation
---

[Clicky](./app/overview.md)


Docs, previews, more to come...