Variables
===

Variables in CrossChUX are located in the `./src/variables/` directory and are organized by type. 
Many of the related variable values are stored in 
[Sass Maps](http://sass-lang.com/documentation/file.SASS_REFERENCE.html#maps) to make recalling 
them easier and to provide a way to iterate over all the values of a particular property.

Once established these shouldn’t change too often. If you need to override any of these 
variables, lists, or maps, most are marked as defaults, so you can do so on a per-project basis. 
Read more about Sass defaults
[here](http://sass-lang.com/documentation/file.SASS_REFERENCE.html#variable_defaults_).

#### Variable Categories:

- **Animation**  
  Presets for consistent animation timing and easings
- **Border**  
  Presets for border radius and border widths
- **Colors**  
  CrossChUX palettes
- **Dimensions**  
  Presets for media query breakpoints, container width/height, grid width, container spacin 
  (for margins, padding, and position offsets)*
- **Flex**  
  TBD: will contain flexbox presets as needed
- **Material Angular**  
  Overrides for Angular Material variables, if necessary
- **Position**  
  Presets for z-index
- **Typography**  
  Presets for Font sizing, font weight, and font family
- **Visibility**  
  Presets for opacity
