Functions
===

CrossChUX contains a few utility Sass functions that are used internally and can be incorporated 
into your .scss files as needed.

###to-rem($px, $context: $root-size)
- **$px**: "desired" pixel dimensions
- **$context** (optional): Size context to use for rem conversion  
  *default*: $root-size: 16px

**returns**: rem representation of provided pixel value


###get_color($color, $value)
- **$color**: Color theme name
- **$value**: Color theme value

**returns**: Hex value of the Theme Color
