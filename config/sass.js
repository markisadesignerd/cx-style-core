const path = require('path');
const config = require('./config');

module.exports = {
  file: path.join(config.srcDir, 'crosschux.scss'),
};
