const fs = require('fs');
const path = require('path');
const config = require('./config');

fs.readdirSync(__dirname)
  .filter((filename) => { return /(index|config)\.js$/.test(filename) !== true; })
  .forEach((filename) => {
    config[path.basename(filename, path.extname(filename))] = require(path.join(__dirname, filename));
  });

module.exports = config;
